use std::fmt;
use std::str::Chars;

/// A character iterator with an internal buffer which provides lots
/// of useful methods for tokenizing unicode charcter inputs as part
/// of the parsing process.
pub struct CharScanner<'a, const N: usize> {
    iter: Chars<'a>,
    queue: CharQueue<N>,
    iter_index: usize,
}
impl<'a, const N: usize> CharScanner<'a, N> {
    /// Create a new `CharScanner` from the provided `Chars` character
    /// iterator.
    pub fn new(iter: Chars<'a>) -> CharScanner<'a, N> {
        CharScanner {
            iter,
            queue: CharQueue::new(),
            iter_index: 0,
        }
    }

    /// Returns then number of characters which have been consumed by
    /// this `CharScanner`.
    pub fn index(&self) -> usize {
        self.iter_index - self.queue.len()
    }

    /// Peek a look at the next character without consuming it. If
    /// there are no more characters then `None` is returned.
    pub fn peek(&mut self) -> Option<char> {
        if self.queue.is_empty() {
            if let Some(c) = self.iter_next() {
                self.queue.add(c);

                Some(c)
            } else {
                None
            }
        } else {
            self.queue.peek()
        }
    }

    /// Peek ahead `ahead` places. This means this method is exactly
    /// the same as `peek()` when `ahead = 0`. If `ahead` is bigger
    /// than the maximum number of characters in the queue then an
    /// error is returned.
    pub fn peek_ahead(&mut self, ahead: usize) -> Result<Option<char>, Error> {
        if ahead >= self.queue.capacity() {
            return Err(Error::PeekAheadValueIsGreaterThanQueueCapacity);
        }

        if !self.fill_queue(ahead + 1) {
            // There are not enough characters left to fill the queue
            // to the requested ahead value, so we return Ok(None).
            return Ok(None);
        }

        Ok(self.queue.peek_ahead(ahead))
    }

    /// Returns `true` if the next characters provided by the scanner
    /// will match `sequence`. This method does not consume any
    /// characters.
    pub fn peek_sequence(&mut self, sequence: &[char]) -> Result<bool, Error> {
        if sequence.len() > self.queue.capacity() {
            return Err(Error::PeekAheadValueIsGreaterThanQueueCapacity);
        }

        if !self.fill_queue(sequence.len()) {
            // There are not enough characters left to fill the queue
            // enough to match the provided sequence.
            return Ok(false);
        }

        Ok(self.queue.peek_sequence(sequence))
    }

    /// Consume the next character if it matches `c` returning `true`
    /// if a match occurred, `false` otherwise.
    pub fn munch(&mut self, c: char) -> bool {
        if self.queue.is_empty() {
            match self.iter_next() {
                Some(next) if next == c => true,
                Some(next) => {
                    self.queue.add(next);

                    false
                }
                None => false,
            }
        } else {
            self.queue.munch(c)
        }
    }

    /// Consume characters while they pass `test`. Returns `true` if
    /// at least one character was consumned, `false` if the next
    /// character did not pass `test`.
    pub fn munch_while(&mut self, test: fn(char) -> bool) -> bool {
        let mut matched: bool = false;

        loop {
            if self.queue.is_empty() {
                match self.iter_next() {
                    Some(c) if test(c) => {
                        matched = true;
                    }
                    Some(c) => {
                        self.queue.add(c);

                        return matched;
                    }
                    None => {
                        return matched;
                    }
                }
            } else if self.queue.munch_if(test) {
                matched = true;
            } else {
                return matched;
            }
        }
    }

    /// Returns `true` if the next characters match `sequence`,
    /// `false` otherwise. If every character in `sequence` is matched
    /// then they are all consumed, otherwise no characters are
    /// consumed.
    pub fn munch_sequence(&mut self, sequence: &[char]) -> bool {
        if !self.fill_queue(sequence.len()) {
            // There are not enough remaining input characters to fill
            // the queue to the same length as `sequence`, so no match
            // is possible.
            return false;
        }

        self.queue.munch_sequence(sequence)
    }

    /// Returns the next character if it passes `test`. If `None` is
    /// returned then no characters are consumed.
    pub fn next_if(&mut self, test: fn(char) -> bool) -> Option<char> {
        match self.peek() {
            Some(c) if test(c) => {
                Some(self.next().unwrap()) // TODO use skip
            }
            _ => None,
        }
    }
    /// Create a `String` with initial capacity `capacity` and fill it
    /// with characters (consuming them) while they pass `test`. If a
    /// non-empty `String` results it is returned, otherwise `None` is
    /// returned. Any returned `String` is shrunk to fit its contents.
    pub fn read_while(&mut self, capacity: usize, test: fn(char) -> bool) -> Option<String> {
        let mut s: String = String::with_capacity(capacity);

        loop {
            match self.peek() {
                Some(c) if test(c) => s.push(self.next().unwrap()),
                _ => break,
            }
        }

        if s.is_empty() {
            return None;
        }

        s.shrink_to_fit();

        Some(s)
    }

    /// Create a `String` with initial capacity `capacity` and fill it
    /// with characters (consuming them) while they pass `test` until
    /// a sequence matching the terminating sequence is reached. The
    /// terminating sequence is consumed but not returned as part of
    /// the `String`. Errors occur if there is an illegal character
    /// e.g. one which does not pass `test` or if no terminating
    /// sequence is reached before the end of the character stream.
    pub fn read_while_until_terminating_sequence(
        &mut self,
        capacity: usize,
        test: fn(char) -> bool,
        terminating_sequence: &[char],
    ) -> Result<String, Error> {
        let mut s: String = String::with_capacity(capacity);

        loop {
            if self.munch_sequence(terminating_sequence) {
                s.shrink_to_fit();

                return Ok(s);
            }

            match self.peek() {
                Some(c) if test(c) => s.push(self.next().unwrap()), // TODO replace with skip
                Some(_c) => return Err(Error::IllegalCharacter),
                None => return Err(Error::MissingTerminatingSequence),
            }
        }
    }

    /// Create a `String` with initial capacity `capacity` and fill it
    /// with characters (consuming them) while they pass `test`. If
    /// the unless sequence is consumed then an error is returned.
    pub fn read_while_unless(
        &mut self,
        capacity: usize,
        test: fn(char) -> bool,
        unless_sequence: &[char],
    ) -> Result<String, Error> {
        let mut s: String = String::with_capacity(capacity);

        loop {
            if self.munch_sequence(unless_sequence) {
                return Err(Error::MunchedIllegalSequence);
            }

            match self.peek() {
                Some(c) if test(c) => s.push(self.next().unwrap()), // TODO replace with skip
                _ => return Ok(s),
            }
        }
    }

    /// Create a `String` in the same manor as `read_while()` except
    /// we have a special case test of `start_test` for the first
    /// character, subsequent characters must pass `test`.
    pub fn read_while_with_special_start(
        &mut self,
        capacity: usize,
        start_test: fn(char) -> bool,
        test: fn(char) -> bool,
    ) -> Option<String> {
        let mut s: String;

        match self.peek() {
            Some(c) if start_test(c) => {
                s = String::with_capacity(capacity);
                s.push(self.next().unwrap());
            }
            _ => return None,
        }

        loop {
            match self.peek() {
                Some(c) if test(c) => s.push(self.next().unwrap()),
                _ => break,
            }
        }

        s.shrink_to_fit();

        Some(s)
    }

    /// Read a decimal and return it as a `u32`. If no decimal digits
    /// can be read or there is an error converting to a `u32` then an
    /// `Error` will be returned.
    // pub fn read_decimal(&mut self) -> Result<u32, Error> {
    //     if let Some(s) = self.read_while(8, is_decimal_digit) {
    //         match s.parse::<u32>() {
    //             Ok(u) => Ok(u),
    //             Err(_) => Err(Error::ParseIntFailure),
    //         }
    //     } else {
    //         Err(Error::MissingDecimalDigits)
    //     }
    // }

    /// Read a hexidecimal and return it as a `u32`. If no hexidecimal digits
    /// can be read or there is an error converting to a `u32` then an
    /// `Error` will be returned.
    // pub fn read_hexidecimal(&mut self) -> Result<u32, Error> {
    //     if let Some(s) = self.read_while(8, is_hexidecimal_digit) {
    //         match u32::from_str_radix(&s, 16) {
    //             Ok(u) => Ok(u),
    //             Err(_) => Err(Error::ParseIntFailure),
    //         }
    //     } else {
    //         Err(Error::MissingHexidecimalDigits)
    //     }
    // }

    /// Fill the queue with `length` characters. Returns `true` if
    /// there were enought characters remaining to achieve this,
    /// `false` otherwise. This is useful when we want to attempt
    /// peeking or matching a sequence.
    ///
    /// *Warning:* This method will panic if `length` is greater than
    /// the `queue` capacity.
    fn fill_queue(&mut self, length: usize) -> bool {
        if length > self.queue.capacity() {
            panic!("Can not fill queue to length greater than capacity.");
        }

        let n = length.saturating_sub(self.queue.len());
        for _ in 0..n {
            if let Some(c) = self.iter_next() {
                self.queue.add(c);
            } else {
                return false;
            }
        }

        true
    }

    /// Get the next character from this `CharScanner`'s input `Chars`
    /// iterator. This method should be used when we would normally
    /// want the next character from the input source because it
    /// automatically increments `iter_index`.
    fn iter_next(&mut self) -> Option<char> {
        if let Some(c) = self.iter.next() {
            self.iter_index += 1;

            Some(c)
        } else {
            None
        }
    }
}
impl<'a, const N: usize> Iterator for CharScanner<'a, N> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        if self.queue.is_empty() {
            self.iter_next()
        } else {
            self.queue.next()
        }
    }
}
impl<'a, const N: usize> fmt::Debug for CharScanner<'a, N> {
    /// We customise `Debug` so that it will display the
    /// pretty-printed queue, returned by `Display`.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Scanner")
            .field("iter_index", &self.iter_index)
            .field("index", &self.index())
            .field("queue", &self.queue)
            .finish()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // #[test]
    // fn peek() {
    //     let chars = "0123456789ABCDEFGHIJK".chars();
    //     let mut scanner: CharScanner<16> = CharScanner::new(chars);

    //     assert_eq!(scanner.peek(), Some('0'));
    //     assert_eq!(scanner.peek_ahead(0), Ok(Some('0')));
    //     assert_eq!(scanner.peek_ahead(1), Ok(Some('1')));
    //     assert_eq!(scanner.peek_ahead(2), Ok(Some('2')));
    //     assert_eq!(scanner.peek_ahead(3), Ok(Some('3')));
    //     assert_eq!(scanner.peek_ahead(4), Ok(Some('4')));
    //     assert_eq!(scanner.peek_ahead(5), Ok(Some('5')));
    //     assert_eq!(scanner.peek_ahead(6), Ok(Some('6')));
    //     assert_eq!(scanner.peek_ahead(7), Ok(Some('7')));
    //     assert_eq!(scanner.peek_ahead(8), Ok(Some('8')));
    //     assert_eq!(scanner.peek_ahead(9), Ok(Some('9')));
    //     assert_eq!(scanner.peek_ahead(10), Ok(Some('A')));
    //     assert_eq!(scanner.peek_ahead(11), Ok(Some('B')));
    //     assert_eq!(scanner.peek_ahead(12), Ok(Some('C')));
    //     assert_eq!(scanner.peek_ahead(13), Ok(Some('D')));
    //     assert_eq!(scanner.peek_ahead(14), Ok(Some('E')));
    //     assert_eq!(scanner.peek_ahead(15), Ok(Some('F')));
    //     assert!(scanner.peek_ahead(16).is_err());
    //     assert_eq!(
    //         scanner.peek_sequence(&[
    //             '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    //         ]),
    //         Ok(true)
    //     );
    // }

    // #[test]
    // fn munch_while() {
    //     let chars = "ABCDEFGH  123X".chars();
    //     let mut scanner = CharScanner::new(chars);

    //     assert_eq!(scanner.munch_while(is_hexidecimal_digit), true);
    //     assert_eq!(scanner.next(), Some('G'));
    //     assert_eq!(scanner.next(), Some('H'));
    //     assert_eq!(scanner.munch_while(is_whitespace), true);
    //     assert_eq!(scanner.next(), Some('1'));
    //     assert_eq!(scanner.munch_while(is_decimal_digit), true);
    //     assert_eq!(scanner.munch_while(is_decimal_digit), false);
    //     assert_eq!(scanner.next(), Some('X'));
    //     assert_eq!(scanner.next(), None);

    //     let chars = "A      B       C123DEFGH    I".chars();
    //     let mut scanner = CharScanner::new(chars);

    //     assert_eq!(scanner.munch_while(is_hexidecimal_digit), true);
    //     assert_eq!(scanner.munch_while(is_whitespace), true);
    //     assert_eq!(scanner.next(), Some('B'));
    //     assert_eq!(scanner.munch_while(is_whitespace), true);
    //     scanner.fill_queue(12);
    //     assert_eq!(scanner.next(), Some('C'));
    //     assert_eq!(scanner.munch_while(is_decimal_digit), true);
    //     assert_eq!(scanner.munch_while(is_decimal_digit), false);
    //     assert_eq!(scanner.next(), Some('D'));
    // }

    #[test]
    fn scanner_test() {
        let chars = "abcdefghijklmnopqrstuvwxyz".chars();
        let mut scanner: CharScanner<16> = CharScanner::new(chars);

        assert_eq!(scanner.next(), Some('a'));
        assert_eq!(scanner.next(), Some('b'));
        assert_eq!(scanner.munch('c'), true);
        assert_eq!(scanner.munch('d'), true);

        assert_eq!(scanner.munch('A'), false);
        let mut buffer = ['\0'; 16];
        buffer[0] = 'e';

        assert_eq!(scanner.munch('B'), false);
        assert_eq!(scanner.munch('e'), true);
        assert_eq!(scanner.munch('f'), true);
        assert_eq!(scanner.munch('g'), true);
        assert_eq!(scanner.munch('h'), true);

        assert_eq!(scanner.munch('C'), false);
        assert_eq!(scanner.munch('D'), false);
        assert_eq!(scanner.munch('E'), false);
        assert_eq!(scanner.munch('F'), false);
        assert_eq!(scanner.munch('G'), false);

        assert_eq!(scanner.munch('i'), true);

        let array = ['j', 'k', 'l', 'm'];
        assert_eq!(scanner.munch_sequence(&array), true);

        assert_eq!(scanner.munch('n'), true);

        let array = ['H', 'I', 'J', 'K'];
        assert_eq!(scanner.munch_sequence(&array), false);

        assert_eq!(scanner.munch('o'), true);
    }

    #[test]
    fn munch_sequence() {
        let chars = "ABCabcdef".chars();
        let mut scanner: CharScanner<16> = CharScanner::new(chars);
        assert!(scanner.munch('A'));
        assert!(scanner.munch('B'));
        assert!(scanner.munch('C'));
        assert!(!scanner.munch('X'));
        assert!(scanner.munch_sequence(&['a', 'b', 'c', 'd', 'e', 'f']));

        let chars = "ABCabc".chars();
        let mut scanner: CharScanner<16> = CharScanner::new(chars);
        assert!(scanner.munch('A'));
        assert!(scanner.munch('B'));
        assert!(scanner.munch('C'));
        assert!(!scanner.munch('X'));
        assert!(!scanner.munch_sequence(&['a', 'b', 'c', 'd', 'e', 'f']));
    }
}

#[derive(Debug)]
pub enum Error {
    IllegalCharacter,
    MissingTerminatingSequence,
    MunchedIllegalSequence,
    PeekAheadValueIsGreaterThanQueueCapacity,
}
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error")
    }
}

/// Represents a queue of characters, used by `CharScanner` to "hold on"
/// to characters which it has pulled from its iterator source, but
/// which hasn't been consumed yet. This is useful when you are
/// wanting to scan for multi-character sequences in XML markup like
/// `"<?xml"`, `"<!DOCTYPE"`, etc.
///
/// The internal representation of the queue is a ring buffer
/// implemented as a `char` array.
///
/// It is important to note that this is not a public struct and it is
/// expected to be used only be the `CharScanner` struct. It has
/// certain methods which will panic if they have not been "set up" in
/// the correct fashion.
struct CharQueue<const N: usize> {
    queue: [char; N],
    start: usize,
    len: usize,
}
impl<const N: usize> CharQueue<N> {
    /// Create a new empty `ScannerQueue`.
    fn new() -> CharQueue<N> {
        CharQueue {
            queue: ['\0'; N],
            start: 0,
            len: 0,
        }
    }

    /// Get the capacity of this `ScannerQueue`, this should be equal
    /// to the `QUEUE_LENGTH` constant.
    fn capacity(&self) -> usize {
        N
    }

    /// Returns `true` if there are currently no charactersin the queue.
    fn is_empty(&self) -> bool {
        self.len == 0
    }

    /// Returns `true` if the queue is currently full.
    fn is_full(&self) -> bool {
        self.len == N
    }

    /// Returns the number of characters currently in the queue.
    fn len(&self) -> usize {
        self.len
    }

    /// Return the incremented value of `index`. The internal
    /// representation of the queue is a ring buffer, so the index
    /// "loops around".
    fn increment_index(&self, index: usize, increment: usize) -> usize {
        (index + increment) % N
    }

    /// Increment the `start` index.
    fn increment_start(&mut self) {
        self.start = self.increment_index(self.start, 1);
    }

    /// Add `c` to the back of the queue.
    ///
    /// *Warning:* This method will panic if the queue is full.
    fn add(&mut self, c: char) {
        if self.is_full() {
            panic!("Attempted to add a character a full queue.");
        }

        self.queue[self.increment_index(self.start, self.len)] = c;
        self.len += 1;
    }

    /// Peek a look at the character at the front of the queue,
    /// without consuming it. If the queue is empty then `None` is
    /// returned.
    fn peek(&self) -> Option<char> {
        if self.is_empty() {
            None
        } else {
            Some(self.queue[self.start])
        }
    }

    /// Peek ahead `ahead` places. This means this method is exactly
    /// the same as `peek()` when `ahead = 0`. If `ahead` is bigger
    /// than the number of characters in the queue then `None` is
    /// returned.
    fn peek_ahead(&self, ahead: usize) -> Option<char> {
        if ahead < self.len {
            Some(self.queue[self.increment_index(self.start, ahead)])
        } else {
            None
        }
    }

    /// Returns `true` if the front of the queue matches the
    /// characters in `sequence`.
    ///
    /// *Warning:* this method must be "set up" correctly, typically
    /// with `scanner.fill_queue(sequence.len())`, as it will panic if
    /// the queue length is shorted than the sequence length.
    fn peek_sequence(&self, sequence: &[char]) -> bool {
        if sequence.len() > self.len {
            panic!("Sequence length is greater than queue length.");
        }

        for (i, c) in sequence.iter().enumerate() {
            if self.queue[self.increment_index(self.start, i)] != *c {
                return false;
            }
        }

        true
    }

    /// Consume the first character in the queue if it matches `c`.
    ///
    /// *Warning:* This method will panic if the queue is empty.
    fn munch(&mut self, c: char) -> bool {
        if self.is_empty() {
            panic!("Queue is empty.")
        }

        if self.queue[self.start] == c {
            self.increment_start();
            self.len -= 1;
            return true;
        }

        false
    }

    /// Consume the first character in the queue if it passes `test`.
    ///
    /// *Warning:* This method will panic if the queue is empty.
    fn munch_if(&mut self, test: fn(char) -> bool) -> bool {
        if self.is_empty() {
            panic!("Queue is empty.")
        }

        if test(self.queue[self.start]) {
            self.increment_start();
            self.len -= 1;
            return true;
        }

        false
    }

    /// Consume the front of the queue if it matches `sequence`.
    ///
    /// *Warning:* this method must be "set up" correctly, typically
    /// with `scanner.fill_queue(sequence.len())`, as it will panic if
    /// the queue length is shorted than the sequence length.
    fn munch_sequence(&mut self, sequence: &[char]) -> bool {
        if sequence.len() > self.len() {
            panic!("Sequence length is greater than queue length.");
        }

        let old_start = self.start;

        for c in sequence {
            if self.queue[self.start] != *c {
                self.start = old_start;

                return false;
            }

            self.increment_start();
        }

        self.len -= sequence.len();

        true
    }
}
impl<const N: usize> Iterator for CharQueue<N> {
    type Item = char;

    /// Consume the next character from the front of the
    /// queue. Returns `None` if the queue is empty.
    fn next(&mut self) -> Option<Self::Item> {
        if self.is_empty() {
            return None;
        }

        let c = self.queue[self.start];
        self.increment_start();
        self.len -= 1;

        Some(c)
    }
}
impl<const N: usize> fmt::Display for CharQueue<N> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut s = String::with_capacity(self.len());

        for i in 0..self.len {
            s.push(self.queue[self.increment_index(self.start, i)]);
        }

        write!(f, "{}", s)
    }
}
impl<const N: usize> fmt::Debug for CharQueue<N> {
    /// We customise `Debug` so that it will display the
    /// pretty-printed queue, returned by `Display`.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pretty_queue = format!("{}", self);

        f.debug_struct("ScannerQueue")
            .field("pretty_queue", &pretty_queue)
            .field("queue", &self.queue)
            .field("start", &self.start)
            .field("len", &self.len)
            .field("capacity", &self.queue.len())
            .field("is_empty", &self.is_empty())
            .field("is_full", &self.is_full())
            .finish()
    }
}

#[cfg(test)]
mod test_scanner_queue {
    use super::*;

    fn init_full_queue() -> CharQueue<16> {
        let mut queue = CharQueue::new();
        queue.add('0');
        queue.add('1');
        queue.add('2');
        queue.add('3');
        queue.add('4');
        queue.add('5');
        queue.add('6');
        queue.add('7');
        queue.add('8');
        queue.add('9');
        queue.add('A');
        queue.add('B');
        queue.add('C');
        queue.add('D');
        queue.add('E');
        queue.add('F');

        queue
    }

    #[test]
    fn new() {
        let queue: CharQueue<16> = CharQueue::new();
        assert_eq!(queue.capacity(), 16);
        assert!(queue.is_empty());
        assert!(!queue.is_full());
        assert_eq!(queue.len(), 0);
    }

    #[test]
    fn increment_index() {
        let queue: CharQueue<16> = CharQueue::new();
        assert_eq!(queue.capacity(), 16);
        assert_eq!(queue.increment_index(0, 0), 0);
        assert_eq!(queue.increment_index(0, 1), 1);
        assert_eq!(queue.increment_index(0, 2), 2);
        assert_eq!(queue.increment_index(1, 2), 3);
        assert_eq!(queue.increment_index(16 - 1, 1), 0);
    }

    #[test]
    fn add() {
        let mut queue: CharQueue<16> = CharQueue::new();
        assert!(queue.is_empty());
        assert!(!queue.is_full());
        assert_eq!(queue.len(), 0);

        queue.add('0');
        assert!(!queue.is_empty());
        assert!(!queue.is_full());
        assert_eq!(queue.len(), 1);

        queue.add('1');
        assert!(!queue.is_empty());
        assert!(!queue.is_full());
        assert_eq!(queue.len(), 2);

        queue.add('2');
        queue.add('3');
        queue.add('4');
        queue.add('5');
        queue.add('6');
        queue.add('7');
        queue.add('8');
        queue.add('9');
        queue.add('A');
        queue.add('B');
        queue.add('C');
        queue.add('D');
        queue.add('E');
        queue.add('F');
        assert_eq!(format!("{}", queue), "0123456789ABCDEF");
        assert!(!queue.is_empty());
        assert!(queue.is_full());
        assert_eq!(queue.len(), 16);
    }

    #[test]
    fn peek() {
        let queue = init_full_queue();
        assert_eq!(queue.peek(), Some('0'));
        assert_eq!(queue.peek_ahead(0), Some('0'));
        assert_eq!(queue.peek_ahead(1), Some('1'));
        assert_eq!(queue.peek_ahead(2), Some('2'));
        assert_eq!(queue.peek_ahead(3), Some('3'));
        assert_eq!(queue.peek_ahead(4), Some('4'));
        assert_eq!(queue.peek_ahead(5), Some('5'));
        assert_eq!(queue.peek_ahead(6), Some('6'));
        assert_eq!(queue.peek_ahead(7), Some('7'));
        assert_eq!(queue.peek_ahead(8), Some('8'));
        assert_eq!(queue.peek_ahead(9), Some('9'));
        assert_eq!(queue.peek_ahead(10), Some('A'));
        assert_eq!(queue.peek_ahead(11), Some('B'));
        assert_eq!(queue.peek_ahead(12), Some('C'));
        assert_eq!(queue.peek_ahead(13), Some('D'));
        assert_eq!(queue.peek_ahead(14), Some('E'));
        assert_eq!(queue.peek_ahead(15), Some('F'));
        assert_eq!(queue.peek_ahead(16), None);
        assert_eq!(queue.peek_sequence(&['0', '1']), true);
        assert_eq!(queue.peek_sequence(&['0', '1', '2']), true);
        assert_eq!(
            queue.peek_sequence(&[
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
            ]),
            true
        );
    }

    #[test]
    fn munch() {
        let mut queue = init_full_queue();
        assert!(queue.is_full());
        assert_eq!(queue.len(), 16);
        assert!(queue.munch('0'));
        assert!(!queue.is_full());
        assert_eq!(queue.len(), 15);
        assert!(!queue.munch('X'));
        assert_eq!(queue.len(), 15);
        assert!(queue.munch('1'));
        assert_eq!(queue.len(), 14);
    }
}
